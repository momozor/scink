(defsystem "scink"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ("parenscript"
               "spinneret"
               "lass")
  :components ((:module "src"
                        :components
                        ((:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "scink/tests"))))

(defsystem "scink/tests"
  :author ""
  :license ""
  :depends-on ("scink"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for scink"
  :perform (test-op (op c) (symbol-call :rove :run c)))
