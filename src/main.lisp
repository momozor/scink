;;;; Scink - A SPA HTML5 front-end template using Common Lisp stack,
;;;; VueJS and MaterialeCSS.
;;;;
;;;; Copyright (C) <YEAR> <AUTHOR> <example@email.com>
;;;;
;;;; For license, see LICENSE file for further details.

(in-package :cl-user)
(defpackage scink
  (:use :cl :parenscript)
  (:import-from :spinneret
                :with-html-string)
  (:import-from :lass
                :compile-and-write)
  (:import-from :asdf
                :system-relative-pathname)
  (:export :build))
(in-package :scink)

;;; Constants.

;; Change your preferences here.
(defparameter *app-name* "Scink")
(defparameter *port* "3000")
(defparameter *hostname*
  (format nil "http://localhost:~a" *port*))
(defparameter *index-html-path*
  (system-relative-pathname :scink "public/index.html"))
(defparameter *style-css-path*
  (system-relative-pathname :scink "public/css/style.css"))
(defparameter *script-js-path*
  (system-relative-pathname :scink "public/js/script.js"))

;;; Functions.

(defun compile-lisp-to-html (&optional (html-path *index-html-path*))
  "Compile Spinneret expressions into a file."
  (with-open-file (f html-path
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (format f "~a" (index-html))))

(defun compile-lisp-to-css (&optional (css-path *style-css-path*))
  "Compile LASS expressions into a file."
  (with-open-file (f css-path
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (format f "~a" (style-css))))

(defun compile-lisp-to-javascript (&optional (js-path *script-js-path*))
  "Compile Parentscript expressions into a file."
  (with-open-file (f js-path
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (format f "~a" (script-js))))

(defun build ()
  "Compile HTML, JS and CSS into their own files."
  (compile-lisp-to-html)
  (compile-lisp-to-css)
  (compile-lisp-to-javascript))

(defun index-html ()
  "Main page for Scink."
  (with-html-string
    (:doctype)
    (:html
     (:head
      (:meta :name "viewport" :content "width=device-width, initial-scale=1")
      (:title *app-name*)
      (:link :rel "stylesheet" :href "css/materialize.css")
      (:link :rel "stylesheet" :href "css/style.css" ))
     (:body
      (:div :id "app"
            (:span "Hello from " (:b (:i "{{message}}!"))))
      (:script :src "js/materialize.js")
      (:script :src "js/vue.js")
      (:script :src "js/script.js")))))

(defun style-css ()
  "Main CSS style for Scink."
  (compile-and-write
   '(body
     :text-align center)))

(defun script-js ()
  "Main engine/core for Scink."
  (ps
    (let ((app
           (new
            (*vue
             (create
              "el" "#app"
              "data" (create
                      "message" (lisp *app-name*)))))))
      app)))
