* Scink 
A SPA HTML5 front-end template using Common Lisp stack, VueJS, and MaterializeCSS.

Don't forget to click the star button on Github if you found this template is helpful!

** Features
- Spinneret instead of HAML
- LASS instead of SASS
- Parenscript instead of Coffeescript, Typescript, etc.
- MaterializeCSS instead of Boostrap.
and VueJS!

** Usage
1. git clone this template locally and customize the name, etc according to your needs.
2. Load the template and invoke (scink:build) to build the front-end code.

** License

This template is released under MIT license. See LICENSE file for further details.
