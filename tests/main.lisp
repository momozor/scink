(defpackage scink/tests/main
  (:use :cl
        :scink
        :rove))
(in-package :scink/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :scink)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
